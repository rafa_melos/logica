#include <stdio.h>
#include <stdlib.h>

int main()
{
    int num;
    int i, j, quantidade, escolha;
    char romano[20];

    do
    {
    printf("Insira um numero entre 1 e 3999: \n");
    scanf("%d",&num);

    j = 0;
    quantidade = num/1000;
    for(i=1 ; i<=quantidade ; i ++)
    {
        romano[j] = 'M';
        j++;
    }

    num = num - quantidade*1000;
    quantidade = num/100;

    if(quantidade <= 3)
    {
        for(i=1 ; i<=quantidade ; i++)
        {
            romano[j] = 'C';
            j++;
        }
    }
    else if(quantidade>=5 && quantidade <=8)
    {
        romano[j] = 'D';
        j++;
        for(i=6 ; i<=quantidade ; i++)
        {
            romano[j] = 'C';
            j++;
        }
    }
    else
    {
        romano[j] = 'C';
        j++;
        if(quantidade==4)
            romano[j] = 'D';
        else
            romano[j] = 'M';
        j++;
    }

    num = num - quantidade*100;
    quantidade = num/10;

    if(quantidade <= 3)
    {
        for(i=1 ; i<=quantidade ; i++)
        {
            romano[j] = 'X';
            j++;
        }
    }
    else if(quantidade>=5 && quantidade <=8)
    {
        romano[j] = 'L';
        j++;
        for(i=6 ; i<=quantidade ; i++)
        {
            romano[j] = 'X';
            j++;
        }
    }
    else
    {
        romano[j] = 'X';
        j++;
        if(quantidade==4)
            romano[j] = 'L';
        else
            romano[j] = 'C';
        j++;
    }

    num = num - quantidade*10;
    quantidade = num;

    if(quantidade <= 3)
    {
        for(i=1 ; i<=quantidade ; i++)
        {
            romano[j] = 'I';
            j++;
        }
    }
    else if(quantidade>=5 && quantidade <=8)
    {
        romano[j] = 'V';
        j++;
        for(i=6 ; i<=quantidade ; i++)
        {
            romano[j] = 'I';
            j++;
        }
    }
    else
    {
        romano[j] = 'I';
        j++;
        if(quantidade==4)
            romano[j] = 'V';
        else
            romano[j] = 'X';
        j++;
    }
    romano[j] = '\0';
    printf("%s\n",romano);

    printf("Digite 1 para continuar ou 0 para sair.\n");
    scanf("%d",&escolha);
    } while(escolha!=0);
    return 0;
}
