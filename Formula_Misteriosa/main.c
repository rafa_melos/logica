#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    unsigned long int entrada;
    int cont, i, escolha;
    char ent[100], aux;

    escolha = 0;
    do
    {
        printf("Insira a entrada: \n");
        scanf("%ld",&entrada);

        itoa(entrada, ent, 10);

        aux = ent[0];
        cont = 1;
        for(i=1 ; i<strlen(ent) ; i++)
        {
            if(ent[i] == aux)
            {
                cont++;
            }
            else
            {
                printf("%d%c",cont,aux);
                aux = ent[i];
                cont = 1;
            }
        }
        printf("%d%c\n",cont,aux);
        printf("Digite 1 para continuar ou 0 para sair.\n");
        scanf("%d",&escolha);
    }
    while(escolha!=0);
    return 0;
}
