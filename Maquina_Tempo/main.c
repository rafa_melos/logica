#include <stdio.h>
#include <stdlib.h>

int main()
{
    int dia, mes, ano;
    int bissexto, valido;
    int escolha;

    do{
    printf("Insira uma data(FORMATO: DD MM AAAA): \n");
    scanf("%d %d %d",&dia,&mes,&ano);

    valido = 0;
    //Checa se o m�s inserido � v�lido, e se o dia inserido � maior ou igual a 1
    if(mes>=1 && mes<=12 && dia>=1)
        valido = 1;

    //Continua a checagem apenas se a informa��o checada for v�lida
    if(valido == 1)
    {
        //Checa se o m�s inserido � fevereiro
        if(mes==2)
        {
            //Checa se o ano � bissexto
            bissexto = 0;
            if(ano%4 == 0)
            {
                bissexto = 1;
                if(ano%100==0 && ano%400!=0)
                {
                    bissexto = 0;
                }
            }

            //Se o ano ��o for bissexto(bissexto = 0) e o dia for maior que 28 a data � inv�lida, ou se o ano for
            //bissexto(bissexto = 1) e o dia for maior que 29 a data � inv�lida
            if((bissexto == 0 && dia>28) || (bissexto==1 && dia>29))
            {
                valido = 0;
            }
        }
        else
        {
            //At� o m�s de Julho(7) os meses pares possuem apenas 30 dias, j� a partir de Agosto(8) s�o os meses impares
            if((mes<=7 && mes%2==0) || (mes>=8 && mes%2!=0))
            {
                if(dia>30)
                    valido = 0;
            }
            else
            {
                if(dia>31)
                    valido = 0;
            }
        }
    }

    if(valido == 1)
        printf("true\n");
    else
        printf("false\n");

    printf("Digite 1 para continuar ou 0 para sair.\n");
    scanf("%d",&escolha);
    } while(escolha != 0);
    return 0;
}
